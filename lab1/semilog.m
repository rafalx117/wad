semilog(years, rabbits, 'Marker', '^', 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'red');
hold on
axis auto
semilogy(years, foxes, 'Marker', '*', 'LineWidth', 1.5, 'LineStyle', ':', 'Color', 'blue');
hold off
grid on;
title('Populacja kr�lik�w i lis�w na przestrzeni lat 1960 - 1997')
xlabel('Lata')
ylabel('Liczebno�� populacji')
legend('Kr�liki', 'Lisy', 'Location', 'Southeast')