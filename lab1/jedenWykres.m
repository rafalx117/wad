plot(years, rabbits, 'Marker', '^', 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'red');
hold on
plot(years, foxes, 'Marker', '*', 'LineWidth', 1.5, 'LineStyle', ':', 'Color', 'blue');
hold off
grid on;
title('Populacja kr�lik�w i lis�w na przestrzeni lat 1960 - 1997')
xlabel('Lata')
ylabel('Liczebno�� populacji')
legend('Kr�liki', 'Lisy', 'Location', 'Northwest');

%dwie osie
figure();
hold on;
yyaxis left;
plot(years, rabbits, 'Marker', '^', 'LineWidth', 1.5, 'LineStyle', '--', 'Color', 'red');
ylabel('Liczebno�� populacji kr�lik�w')

yyaxis right;
plot(years, foxes, 'Marker', '*', 'LineWidth', 1.5, 'LineStyle', ':', 'Color', 'blue');

grid on;
title('Populacja kr�lik�w i lis�w na przestrzeni lat 1960 - 1997')
xlabel('Lata')
ylabel('Liczebno�� populacji lis�w')
legend('Kr�liki', 'Lisy', 'Location', 'Northwest');
hold off;