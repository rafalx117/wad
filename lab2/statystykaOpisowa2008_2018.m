przygotowanieDanych;
disp('2007 - 2018')
vmax = max(data2008_2018.spots)
vmean = mean(data2008_2018.spots, 'omitnan')
vmedian = median(data2008_2018.spots, 'omitnan')
vmin = min(data2008_2018.spots)
vmode = mode(data2008_2018.spots)
vstd = std(data2008_2018.spots, 'omitnan')
vvar = var(data2008_2018.spots, 'omitnan')