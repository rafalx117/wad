przygotowanieDanych;
samplesPerYear = 365.2727;
yearPerPeriod = 11;
samplesPerPeriod = ceil(yearPerPeriod * samplesPerYear);
spots = data1950_2004.spots;

p1 = spots(1:4018);
p2 = spots(4019:8036);
p3 = spots(8037:12054);
p4 = spots(12055:16072);
p5 = spots(16072:20089);

period = [p1;p2;p3;p4;p5];
group = [ones((size(p1)));
    2*ones((size(p2)));
    3*ones((size(p3)));
    4*ones((size(p4)));
    5*ones((size(p5)))];

figure();
boxplot(period, group, 'Labels', {'1950-60','1961-71','1972-82','1983-94','1994-2004'});
title('Przedzia� czasu 1950 - 2005');
%set(gca, 'xlabel',{'a','b','c','d','e'});
xlabel('Okresy');
ylabel('Ilo�� plam');
grid on;



