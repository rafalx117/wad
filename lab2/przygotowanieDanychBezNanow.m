importRawDataFromFile;
correctedData = rawData;

rows1950_2004 = correctedData.year>=1950 & correctedData.year<=2004;
rows1973_2005 = correctedData.year>=1973 & correctedData.year<=2005;
rows2008_2018 = correctedData.year>=2008 & correctedData.year<=2018;

columnsToDisplay={'time','spots'};

data1950_2004=correctedData(rows1950_2004, columnsToDisplay);
data1973_2005=correctedData(rows1973_2005, columnsToDisplay);
data2008_2018=correctedData(rows2008_2018, columnsToDisplay);