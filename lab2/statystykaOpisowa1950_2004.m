przygotowanieDanych;
disp('1950 - 2004')
vmax = max(data1950_2004.spots)
vmean = mean(data1950_2004.spots, 'omitnan')
vmedian = median(data1950_2004.spots, 'omitnan')
vmin = min(data1950_2004.spots)
vmode = mode(data1950_2004.spots)
vstd = std(data1950_2004.spots, 'omitnan')
vvar = var(data1950_2004.spots, 'omitnan')

