przygotowanieDanych;
samplesPerYear = 365.2727;
yearPerPeriod = 11;
samplesPerPeriod = floor(yearPerPeriod * samplesPerYear);
spots = data1973_2005.spots;

p1 = spots(1:samplesPerPeriod);
p2 = spots(samplesPerPeriod+1:2*samplesPerPeriod);
p3 = spots(2*samplesPerPeriod+1:3*samplesPerPeriod);

period = [p1;p2;p3];
group = [ones((size(p1)));
    2*ones((size(p2)));
    3*ones((size(p3)))];

figure();
boxplot(period, group, 'Labels', {'1973-83','1984-94','1995-2005'});
title('Przedzia� czasu 1973 - 2005');
xlabel('Okresy');
ylabel('Ilo�� plam');
grid on;



