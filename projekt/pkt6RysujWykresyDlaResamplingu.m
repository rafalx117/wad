function pkt6RysujWykresyDlaResamplingu(channelData1, channelData2, channelData3, semilogyTitle)

%wykres przej�� przez zero
figure();
hold on;
semilogy(cell2mat(channelData1(:,4))); 
semilogy(cell2mat(channelData2(:,4))); 
semilogy(cell2mat(channelData3(:,4))); 
hold off;
title(strcat(semilogyTitle, ": ", 'Liczba przej�� przez o� X w oknach 1-sekundowych'));
xlabel('Okna 1-sekundowe');
ylabel('Liczba przej��');
legend('Orygina�','Resample 0.5kS', 'Resample 4kS');
grid on;

%wykres przej�� przez zero dla 1 pochodnej
figure();
hold on;
semilogy(cell2mat(channelData1(:,5))); 
semilogy(cell2mat(channelData2(:,5))); 
semilogy(cell2mat(channelData3(:,5))); 
hold off;
title(strcat(semilogyTitle, ": ", 'Liczba przej�� przez o� X w oknach 1-sekundowych dla pierwszej pochodnej'));
xlabel('Okna 1-sekundowe');
ylabel('Liczba przej��');
legend('Orygina�','Resample 0.5kS', 'Resample 4kS');
grid on;
end