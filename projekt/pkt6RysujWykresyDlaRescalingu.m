function pkt6RysujWykresyDlaRescalingu(channelData1, channelData2, channelData3, semilogyTitle)
%wykres �redniej
figure();
hold on;
semilogy(cell2mat(channelData1(:,2))); 
semilogy(cell2mat(channelData2(:,2))); 
semilogy(cell2mat(channelData3(:,2))); 
hold off;
title(strcat(semilogyTitle, ": ", 'Warto�� �rednia w oknach 1-sekundowych'));
xlabel('Okna 1-sekundowe');
ylabel('Warto��');
legend('Orygina�', 'Rescale 1/2', 'Rescale 1/4');
grid on;

%wykres wariancji
figure();
hold on;
semilogy(cell2mat(channelData1(:,3))); 
semilogy(cell2mat(channelData2(:,3))); 
semilogy(cell2mat(channelData3(:,3))); 
hold off;
title(strcat(semilogyTitle, ": ", 'Warto�� wariancji w oknach 1-sekundowych'));
xlabel('Okna 1-sekundowe');
ylabel('Warto��');
legend('Orygina�', 'Rescale 1/2', 'Rescale 1/4');
grid on;
end