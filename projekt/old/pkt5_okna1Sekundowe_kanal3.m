pkt5_okna1Sekundowe_dane;

%--------------- KANA� 3 ------------------------
%wykres �redniej
figure();
plot(channel3Windows{2}); 
title('Warto�� �rednia w oknach 1-sekundowych');
xlabel('Okna 1-sekundowe');
ylabel('Warto��');
grid on;

%wykres wariancji
figure();
plot(channel3Windows{3}); 
title('Warto�� wariancji w oknach 1-sekundowych');
xlabel('Okna 1-sekundowe');
ylabel('Warto��');
grid on;

%wykres przej�� przez zero
figure();
plot(channel3Windows{4}); 
title('Liczba przej�� przez o� X w oknach 1-sekundowych');
xlabel('Okna 1-sekundowe');
ylabel('Liczba przej��');
grid on;

%wykres przej�� przez zero
figure();
plot(channel3Windows{5}); 
title('Liczba przej�� przez o� X w oknach 1-sekundowych dla pierwszej pochodnej');
xlabel('Okna 1-sekundowe');
ylabel('Liczba przej��');
grid on;
%-----------------------------------------------------