podzialNaKanaly;

vmax = max(channel1)
vmean = mean(channel1, 'omitnan')
vmedian = median(channel1, 'omitnan')
vmin = min(channel1)
vmode = mode(channel1)
vstd = std(channel1, 'omitnan')
vvar = var(channel1, 'omitnan')

statsC1 = cell(7,2);
statsC1{1, 1} = 'mean';
statsC1{1, 2} = 'median';
statsC1{1, 3} = 'min';
statsC1{1, 4} = 'max';
statsC1{1, 5} = 'mode';
statsC1{1, 6} = 'std';
statsC1{1, 7} = 'var';

statsC1{2, 1} = vmean;
statsC1{2, 2} = vmedian;
statsC1{2, 3} = vmin;
statsC1{2, 4} = vmax;
statsC1{2, 5} = vmode;
statsC1{2, 6} = vstd;
statsC1{2, 7} = vvar;

% channel 2
vmax = max(channel2)
vmean = mean(channel2, 'omitnan')
vmedian = median(channel2, 'omitnan')
vmin = min(channel2)
vmode = mode(channel2)
vstd = std(channel2, 'omitnan')
vvar = var(channel2, 'omitnan')


statsC2 = cell(7,2);
statsC2{1, 1} = 'mean';
statsC2{1, 2} = 'median';
statsC2{1, 3} = 'min';
statsC2{1, 4} = 'max';
statsC2{1, 5} = 'mode';
statsC2{1, 6} = 'std';
statsC2{1, 7} = 'var';

statsC2{2, 1} = vmean;
statsC2{2, 2} = vmedian;
statsC2{2, 3} = vmin;
statsC2{2, 4} = vmax;
statsC2{2, 5} = vmode;
statsC2{2, 6} = vstd;
statsC2{2, 7} = vvar;

% channel 3
vmax = max(channel3)
vmean = mean(channel3, 'omitnan')
vmedian = median(channel3, 'omitnan')
vmin = min(channel3)
vmode = mode(channel3)
vstd = std(channel3, 'omitnan')
vvar = var(channel3, 'omitnan')


statsC3 = cell(7,2);
statsC3{1, 1} = 'mean';
statsC3{1, 2} = 'median';
statsC3{1, 3} = 'min';
statsC3{1, 4} = 'max';
statsC3{1, 5} = 'mode';
statsC3{1, 6} = 'std';
statsC3{1, 7} = 'var';

statsC3{2, 1} = vmean;
statsC3{2, 2} = vmedian;
statsC3{2, 3} = vmin;
statsC3{2, 4} = vmax;
statsC3{2, 5} = vmode;
statsC3{2, 6} = vstd;
statsC3{2, 7} = vvar;

% channel 4
vmax = max(channel4)
vmean = mean(channel4, 'omitnan')
vmedian = median(channel4, 'omitnan')
vmin = min(channel4)
vmode = mode(channel4)
vstd = std(channel4, 'omitnan')
vvar = var(channel4, 'omitnan')


statsC4 = cell(7,2);
statsC4{1, 1} = 'mean';
statsC4{1, 2} = 'median';
statsC4{1, 3} = 'min';
statsC4{1, 4} = 'max';
statsC4{1, 5} = 'mode';
statsC4{1, 6} = 'std';
statsC4{1, 7} = 'var';

statsC4{2, 1} = vmean;
statsC4{2, 2} = vmedian;
statsC4{2, 3} = vmin;
statsC4{2, 4} = vmax;
statsC4{2, 5} = vmode;
statsC4{2, 6} = vstd;
statsC4{2, 7} = vvar;

% channel 5
vmax = max(channel5)
vmean = mean(channel5, 'omitnan')
vmedian = median(channel5, 'omitnan')
vmin = min(channel5)
vmode = mode(channel5)
vstd = std(channel5, 'omitnan')
vvar = var(channel5, 'omitnan')


statsC5 = cell(7,2);
statsC5{1, 1} = 'mean';
statsC5{1, 2} = 'median';
statsC5{1, 3} = 'min';
statsC5{1, 4} = 'max';
statsC5{1, 5} = 'mode';
statsC5{1, 6} = 'std';
statsC5{1, 7} = 'var';

statsC5{2, 1} = vmean;
statsC5{2, 2} = vmedian;
statsC5{2, 3} = vmin;
statsC5{2, 4} = vmax;
statsC5{2, 5} = vmode;
statsC5{2, 6} = vstd;
statsC5{2, 7} = vvar;

