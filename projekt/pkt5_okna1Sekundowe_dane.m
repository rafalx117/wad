%splitapply(@mean, channel1, 300); %do weryfikacji
podzialNaKanaly;
samplesCount = size(channel1);
samplesCount = samplesCount(1);
sampleRate = samplesCount / 300;

% ------------------------------- Plik 1 --------------------------------

channel1Windows = mat2cell(channel1, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel2Windows = mat2cell(channel2, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel3Windows = mat2cell(channel3, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel4Windows = mat2cell(channel4, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel5Windows = mat2cell(channel5, sampleRate*(ones(samplesCount/sampleRate,1)), 1);

for i = 1 : 300
   %�rednia
   channel1Windows{i,2} = mean(channel1Windows{i});
   channel2Windows{i,2} = mean(channel2Windows{i});
   channel3Windows{i,2} = mean(channel3Windows{i});
   channel4Windows{i,2} = mean(channel4Windows{i});
   channel5Windows{i,2} = mean(channel5Windows{i});
   
   %wariancja
   channel1Windows{i,3} = var(channel1Windows{i});
   channel2Windows{i,3} = var(channel2Windows{i});
   channel3Windows{i,3} = var(channel3Windows{i});
   channel4Windows{i,3} = var(channel4Windows{i});
   channel5Windows{i,3} = var(channel5Windows{i});   
   
   %liczba przej�� przez zero
   channelMinusMean = channel1Windows{i} - mean(channel1Windows{i});
   channel1Windows{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel2Windows{i} - mean(channel2Windows{i});
   channel2Windows{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel3Windows{i} - mean(channel3Windows{i});
   channel3Windows{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel4Windows{i} - mean(channel4Windows{i});
   channel4Windows{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel5Windows{i} - mean(channel5Windows{i});
   channel5Windows{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   %liczba przej�� przez zero dla 1 pochodnej
   derivative = diff(channel1Windows{i});
   channel1Windows{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel2Windows{i});
   channel2Windows{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel3Windows{i});
   channel3Windows{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel4Windows{i});
   channel4Windows{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel5Windows{i});
   channel5Windows{i,5} = numel(find(diff(sign(derivative))));   
end

%------------------------------------------------------------------------

% ------------------------------- Plik 2 --------------------------------

channel1Windows_file2 = mat2cell(channel1_file2, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel2Windows_file2 = mat2cell(channel2_file2, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel3Windows_file2 = mat2cell(channel3_file2, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel4Windows_file2 = mat2cell(channel4_file2, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel5Windows_file2 = mat2cell(channel5_file2, sampleRate*(ones(samplesCount/sampleRate,1)), 1);

for i = 1 : 300
   %�rednia
   channel1Windows_file2{i,2} = mean(channel1Windows_file2{i});
   channel2Windows_file2{i,2} = mean(channel2Windows_file2{i});
   channel3Windows_file2{i,2} = mean(channel3Windows_file2{i});
   channel4Windows_file2{i,2} = mean(channel4Windows_file2{i});
   channel5Windows_file2{i,2} = mean(channel5Windows_file2{i});
   
   %wariancja
   channel1Windows_file2{i,3} = var(channel1Windows_file2{i});
   channel2Windows_file2{i,3} = var(channel2Windows_file2{i});
   channel3Windows_file2{i,3} = var(channel3Windows_file2{i});
   channel4Windows_file2{i,3} = var(channel4Windows_file2{i});
   channel5Windows_file2{i,3} = var(channel5Windows_file2{i});   
   
   %liczba przej�� przez zero
   channelMinusMean = channel1Windows_file2{i} - mean(channel1Windows_file2{i});
   channel1Windows_file2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel2Windows_file2{i} - mean(channel2Windows_file2{i});
   channel2Windows_file2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel3Windows_file2{i} - mean(channel3Windows_file2{i});
   channel3Windows_file2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel4Windows_file2{i} - mean(channel4Windows_file2{i});
   channel4Windows_file2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel5Windows_file2{i} - mean(channel5Windows_file2{i});
   channel5Windows_file2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   %liczba przej�� przez zero dla 1 pochodnej
   derivative = diff(channel1Windows_file2{i});
   channel1Windows_file2{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel2Windows_file2{i});
   channel2Windows_file2{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel3Windows_file2{i});
   channel3Windows_file2{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel4Windows_file2{i});
   channel4Windows_file2{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel5Windows_file2{i});
   channel5Windows_file2{i,5} = numel(find(diff(sign(derivative))));   
end

%------------------------------------------------------------------------

% ------------------------------- Plik 3 --------------------------------

channel1Windows_file3 = mat2cell(channel1_file3, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel2Windows_file3 = mat2cell(channel2_file3, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel3Windows_file3 = mat2cell(channel3_file3, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel4Windows_file3 = mat2cell(channel4_file3, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel5Windows_file3 = mat2cell(channel5_file3, sampleRate*(ones(samplesCount/sampleRate,1)), 1);

for i = 1 : 300
   %�rednia
   channel1Windows_file3{i,2} = mean(channel1Windows_file3{i});
   channel2Windows_file3{i,2} = mean(channel2Windows_file3{i});
   channel3Windows_file3{i,2} = mean(channel3Windows_file3{i});
   channel4Windows_file3{i,2} = mean(channel4Windows_file3{i});
   channel5Windows_file3{i,2} = mean(channel5Windows_file3{i});
   
   %wariancja
   channel1Windows_file3{i,3} = var(channel1Windows_file3{i});
   channel2Windows_file3{i,3} = var(channel2Windows_file3{i});
   channel3Windows_file3{i,3} = var(channel3Windows_file3{i});
   channel4Windows_file3{i,3} = var(channel4Windows_file3{i});
   channel5Windows_file3{i,3} = var(channel5Windows_file3{i});   
   
   %liczba przej�� przez zero
   channelMinusMean = channel1Windows_file3{i} - mean(channel1Windows_file3{i});
   channel1Windows_file3{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel2Windows_file3{i} - mean(channel2Windows_file3{i});
   channel2Windows_file3{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel3Windows_file3{i} - mean(channel3Windows_file3{i});
   channel3Windows_file3{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel4Windows_file3{i} - mean(channel4Windows_file3{i});
   channel4Windows_file3{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel5Windows_file3{i} - mean(channel5Windows_file3{i});
   channel5Windows_file3{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   %liczba przej�� przez zero dla 1 pochodnej
   derivative = diff(channel1Windows_file3{i});
   channel1Windows_file3{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel2Windows_file3{i});
   channel2Windows_file3{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel3Windows_file3{i});
   channel3Windows_file3{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel4Windows_file3{i});
   channel4Windows_file3{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel5Windows_file3{i});
   channel5Windows_file3{i,5} = numel(find(diff(sign(derivative))));   
end

%------------------------------------------------------------------------

