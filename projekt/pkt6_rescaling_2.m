podzialNaKanaly;
channel1 = channel1 / 4;
channel2 = channel2 / 4;
channel3 = channel3 / 4;
channel4 = channel4 / 4;
channel5 = channel5 / 4;

samplesCount = size(channel1);
samplesCount = samplesCount(1);
sampleRate = samplesCount / 300;

channel1WindowsRescale2 = mat2cell(channel1, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel2WindowsRescale2 = mat2cell(channel2, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel3WindowsRescale2 = mat2cell(channel3, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel4WindowsRescale2 = mat2cell(channel4, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel5WindowsRescale2 = mat2cell(channel5, sampleRate*(ones(samplesCount/sampleRate,1)), 1);

for i = 1 : 300
   %�rednia
   channel1WindowsRescale2{i,2} = mean(channel1WindowsRescale2{i});
   channel2WindowsRescale2{i,2} = mean(channel2WindowsRescale2{i});
   channel3WindowsRescale2{i,2} = mean(channel3WindowsRescale2{i});
   channel4WindowsRescale2{i,2} = mean(channel4WindowsRescale2{i});
   channel5WindowsRescale2{i,2} = mean(channel5WindowsRescale2{i});
   
   %wariancja
   channel1WindowsRescale2{i,3} = var(channel1WindowsRescale2{i});
   channel2WindowsRescale2{i,3} = var(channel2WindowsRescale2{i});
   channel3WindowsRescale2{i,3} = var(channel3WindowsRescale2{i});
   channel4WindowsRescale2{i,3} = var(channel4WindowsRescale2{i});
   channel5WindowsRescale2{i,3} = var(channel5WindowsRescale2{i});   
   
   %liczba przej�� przez zero
   channelMinusMean = channel1WindowsRescale2{i} - mean(channel1WindowsRescale2{i});
   channel1WindowsRescale2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel2WindowsRescale2{i} - mean(channel2WindowsRescale2{i});
   channel2WindowsRescale2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel3WindowsRescale2{i} - mean(channel3WindowsRescale2{i});
   channel3WindowsRescale2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel4WindowsRescale2{i} - mean(channel4WindowsRescale2{i});
   channel4WindowsRescale2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel5WindowsRescale2{i} - mean(channel5WindowsRescale2{i});
   channel5WindowsRescale2{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   %liczba przej�� przez zero dla 1 pochodnej
   derivative = diff(channel1WindowsRescale2{i});
   channel1WindowsRescale2{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel2WindowsRescale2{i});
   channel2WindowsRescale2{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel3WindowsRescale2{i});
   channel3WindowsRescale2{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel4WindowsRescale2{i});
   channel4WindowsRescale2{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel5WindowsRescale2{i});
   channel5WindowsRescale2{i,5} = numel(find(diff(sign(derivative))));   
end



