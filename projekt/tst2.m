figure();
hold on;
plot(cell2mat(channel1Windows(:,4))); 
plot(cell2mat(channel1WindowsResample05kS(:,4))); 
plot(cell2mat(channel1WindowsResample4kS(:,4))); 
plot(cell2mat(channel1WindowsRescale1(:,4))); 
%plot(cell2mat(channel1WindowsRescale2(:,4))); 

%legend('raw','resample 0.5kS', 'resample 4kS', 'rescale 1/2', 'rescale 1/16');

hold off;
