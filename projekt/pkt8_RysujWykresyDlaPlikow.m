function pkt8_RysujWykresyDlaPlikow(channelData_file1, channelData_file2, channelData_file3,  plotName, xlabelName, ylabelName)
%Rozrzut �rednia - wariancja
figure();

subplot(3,2,1);
hold on;
scatter(cell2mat(channelData_file1(:,2)), cell2mat(channelData_file1(:,3))); 
scatter(cell2mat(channelData_file2(:,2)), cell2mat(channelData_file2(:,3))); 
scatter(cell2mat(channelData_file3(:,2)), cell2mat(channelData_file3(:,3))); 
title(strcat(plotName, ": ", 'Rozrzut �redniej wzgl�dem wariancji'));
xlabel('�rednia');
ylabel('Wariancja');
hold off;
grid on;

%------------ �rednia - ilo�� przej�� przez zero ---------------
subplot(3,2,2);

hold on;
scatter(cell2mat(channelData_file1(:,2)), cell2mat(channelData_file1(:,4))); 
scatter(cell2mat(channelData_file2(:,2)), cell2mat(channelData_file2(:,4))); 
scatter(cell2mat(channelData_file3(:,2)), cell2mat(channelData_file3(:,4))); 
title(strcat(plotName, ": ", 'Rozrzut �redniej wzgl�dem liczby przej�� przez zero'));
xlabel('�rednia');
ylabel('Liczba przej�� przez zero');
hold off;
grid on;

%----------- �rednia - ilo�� przej�� przez zero w 1 pochodnej -------
subplot(3,2,3);

hold on;
scatter(cell2mat(channelData_file1(:,2)), cell2mat(channelData_file1(:,5))); 
scatter(cell2mat(channelData_file2(:,2)), cell2mat(channelData_file2(:,5))); 
scatter(cell2mat(channelData_file3(:,2)), cell2mat(channelData_file3(:,5))); 
title(strcat(plotName, ": ", 'Rozrzut �redniej wzgl�dem LPZ w 1. pochodnej'));
xlabel('�rednia');
ylabel('LPZ w 1. pochodnej');
hold off;
grid on;

%--------- Wariancja - liczba przej�� przez zero ------------
subplot(3,2,4);

hold on;
scatter(cell2mat(channelData_file1(:,3)), cell2mat(channelData_file1(:,4))); 
scatter(cell2mat(channelData_file2(:,3)), cell2mat(channelData_file2(:,4))); 
scatter(cell2mat(channelData_file3(:,3)), cell2mat(channelData_file3(:,4))); 
title(strcat(plotName, ": ", 'Rozrzut �redniej wzgl�dem liczby przej�� przez zero'));
xlabel('Wariancja');
ylabel('Liczba przej�� przez zero');
hold off;
grid on;

%--------- Wariancja - liczba przej�� przez zero w 1 pochodnej ------------
subplot(3,2,5);
hold on;
scatter(cell2mat(channelData_file1(:,3)), cell2mat(channelData_file1(:,5))); 
scatter(cell2mat(channelData_file2(:,3)), cell2mat(channelData_file2(:,5))); 
scatter(cell2mat(channelData_file3(:,3)), cell2mat(channelData_file3(:,5))); 
title(strcat(plotName, ": ", 'Rozrzut wariancji wzgl�dem LPZ w 1. pochodnej'));
xlabel('Wariancja');
ylabel('LPZ w 1. pochodnej');
hold off;
grid on;

%-------- LPZ - LPZ w 1 pochodnej --------------------------------------

subplot(3,2,6);

hold on;
scatter(cell2mat(channelData_file1(:,4)), cell2mat(channelData_file1(:,5))); 
scatter(cell2mat(channelData_file2(:,4)), cell2mat(channelData_file2(:,5))); 
scatter(cell2mat(channelData_file3(:,4)), cell2mat(channelData_file3(:,5))); 
title(strcat(plotName, ": ", 'Rozrzut LPZ wzgl�dem LPZ w pierwszej pochodnej'));
xlabel('Liczba przej�� przez zero');
ylabel('LPZ w 1. pochodnej');
hold off;
grid on;
legend('Plik A01R', 'Plik A02R', 'Plik A03R', 'Location', 'Northwest');

end