podzialNaKanaly;

autocorr = cell(5,1);
channels = cell(5,1);
channels{1,1} = channel1;
channels{2,1} = channel2;
channels{3,1} = channel3;
channels{4,1} = channel4;
channels{5,1} = channel5;

for i = 1 : 5
    %vmean = mean(channels{i, 1}, 'omitnan');
    %autocorr{i,1} = xcorr(channels{i, 1} - vmean);
    autocorr{i,1} = xcorr(channels{i, 1});
    figure()
    plot(autocorr{i,1});
    title(strcat("Channel ", int2str(i)));
    
end


