%------- RESAMPLING 0.5kS/s ------------------
podzialNaKanalyResampling05kS; %konwersja oryginalnego pliku oraz podzia� na kana�y

samplesCount = size(channel1);
samplesCount = samplesCount(1);
sampleRate = samplesCount / 300;

channel1WindowsResample05kS = mat2cell(channel1, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel2WindowsResample05kS = mat2cell(channel2, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel3WindowsResample05kS = mat2cell(channel3, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel4WindowsResample05kS = mat2cell(channel4, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel5WindowsResample05kS = mat2cell(channel5, sampleRate*(ones(samplesCount/sampleRate,1)), 1);

for i = 1 : 300
   %�rednia
   channel1WindowsResample05kS{i,2} = mean(channel1WindowsResample05kS{i});
   channel2WindowsResample05kS{i,2} = mean(channel2WindowsResample05kS{i});
   channel3WindowsResample05kS{i,2} = mean(channel3WindowsResample05kS{i});
   channel4WindowsResample05kS{i,2} = mean(channel4WindowsResample05kS{i});
   channel5WindowsResample05kS{i,2} = mean(channel5WindowsResample05kS{i});
   
   %wariancja
   channel1WindowsResample05kS{i,3} = var(channel1WindowsResample05kS{i});
   channel2WindowsResample05kS{i,3} = var(channel2WindowsResample05kS{i});
   channel3WindowsResample05kS{i,3} = var(channel3WindowsResample05kS{i});
   channel4WindowsResample05kS{i,3} = var(channel4WindowsResample05kS{i});
   channel5WindowsResample05kS{i,3} = var(channel5WindowsResample05kS{i});   
   
   %liczba przej�� przez zero
   channelMinusMean = channel1WindowsResample05kS{i} - mean(channel1WindowsResample05kS{i});
   channel1WindowsResample05kS{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel2WindowsResample05kS{i} - mean(channel2WindowsResample05kS{i});
   channel2WindowsResample05kS{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel3WindowsResample05kS{i} - mean(channel3WindowsResample05kS{i});
   channel3WindowsResample05kS{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel4WindowsResample05kS{i} - mean(channel4WindowsResample05kS{i});
   channel4WindowsResample05kS{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel5WindowsResample05kS{i} - mean(channel5WindowsResample05kS{i});
   channel5WindowsResample05kS{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   %liczba przej�� przez zero dla 1 pochodnej
   derivative = diff(channel1WindowsResample05kS{i});
   channel1WindowsResample05kS{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel2WindowsResample05kS{i});
   channel2WindowsResample05kS{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel3WindowsResample05kS{i});
   channel3WindowsResample05kS{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel4WindowsResample05kS{i});
   channel4WindowsResample05kS{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel5WindowsResample05kS{i});
   channel5WindowsResample05kS{i,5} = numel(find(diff(sign(derivative))));   
end

%pkt5RysujWykresy(channel1WindowsResample05kS, 'Resampling 0,5kS/s - Kana� 1')
%pkt5RysujWykresy(channel2WindowsResample05kS, 'Resampling 0,5kS/s - Kana� 2')
%pkt5RysujWykresy(channel3WindowsResample05kS, 'Resampling 0,5kS/s - Kana� 3')
%pkt5RysujWykresy(channel4WindowsResample05kS, 'Resampling 0,5kS/s - Kana� 4')
%pkt5RysujWykresy(channel5WindowsResample05kS, 'Resampling 0,5kS/s - Kana� 5')
%-------------------------------------------