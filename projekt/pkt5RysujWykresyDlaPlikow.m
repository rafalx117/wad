function pkt5RysujWykresyDlaPlikow(channelData_file1, channelData_file2, channelData_file3,  plotName)
%wykres �redniej
figure();
hold on;
plot(cell2mat(channelData_file1(:,2))); 
plot(cell2mat(channelData_file2(:,2))); 
plot(cell2mat(channelData_file3(:,2))); 
title(strcat(plotName, ": ", 'Warto�� �rednia w oknach 1-sekundowych'));
xlabel('Okna 1-sekundowe');
ylabel('Warto��');
hold off;
grid on;
legend('Plik A01R', 'Plik A02R', 'Plik A03R');
fileName = strcat(plotName, " ", 'srednia.png');
saveas(gcf, fileName)

%wykres wariancji
figure();
hold on;
plot(cell2mat(channelData_file1(:,3))); 
plot(cell2mat(channelData_file2(:,3))); 
plot(cell2mat(channelData_file3(:,3)));  
title(strcat(plotName, ": ", 'Warto�� wariancji w oknach 1-sekundowych'));
xlabel('Okna 1-sekundowe');
ylabel('Warto��');
grid on;
legend('Plik A01R', 'Plik A02R', 'Plik A03R');
fileName = strcat(plotName, " ", 'wariancja.png');
saveas(gcf, fileName)

%wykres przej�� przez zero
figure();
hold on;
plot(cell2mat(channelData_file1(:,4))); 
plot(cell2mat(channelData_file2(:,4))); 
plot(cell2mat(channelData_file3(:,4))); 
title(strcat(plotName, ": ", 'Liczba przej�� przez o� X w oknach 1-sekundowych'));
xlabel('Okna 1-sekundowe');
ylabel('Liczba przej��');
hold off;
grid on;
legend('Plik A01R', 'Plik A02R', 'Plik A03R');
fileName = strcat(plotName, " ", 'zera.png');
saveas(gcf, fileName)

%wykres przej�� przez zero dla 1 pochodnej
figure();
hold on;
plot(cell2mat(channelData_file1(:,5))); 
plot(cell2mat(channelData_file2(:,5))); 
plot(cell2mat(channelData_file3(:,5))); 
title(strcat(plotName, ": ", 'Liczba przej�� przez o� X w oknach 1-sekundowych dla pierwszej pochodnej'));
xlabel('Okna 1-sekundowe');
ylabel('Liczba przej��');
hold off;
grid on;
legend('Plik A01R', 'Plik A02R', 'Plik A03R');
fileName = strcat(plotName, " ", 'pochodna.png');
saveas(gcf, fileName)

end