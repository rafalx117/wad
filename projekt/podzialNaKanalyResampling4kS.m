% --------- RESAMPLING -----------
load('data.mat');

originalSamplingRate = 8 * 1024;
resampleRate4kS = 4 * 1024;

resampledData4kS = resample(data, resampleRate4kS, originalSamplingRate);

channel1 = resampledData4kS(:,1);
channel2 = resampledData4kS(:,2);
channel3 = resampledData4kS(:,3);
channel4 = resampledData4kS(:,4);
channel5 = resampledData4kS(:,5);


