podzialNaKanaly;
%corr12 = xcorr(channel1,channel2);
%corr13 = xcorr(channel1,channel3);
%corr14 = xcorr(channel1,channel4);
%corr15 = xcorr(channel1,channel5);
%corr23 = xcorr(channel2,channel3);
%corr24 = xcorr(channel2,channel4);
%corr25 = xcorr(channel2,channel5);
%corr34 = xcorr(channel3,channel4);
%corr35 = xcorr(channel3,channel5);
%corr45 = xcorr(channel4,channel5);

% ---------- opóźnienie plik 1 ------------
channels = cell(5,1);
channels{1,1} = channel1;
channels{2,1} = channel2;
channels{3,1} = channel3;
channels{4,1} = channel4;
channels{5,1} = channel5;
shift = zeros(5);
for i = 1 : 5
    for j = 1 : 5
        shift(i,j) = finddelay(channels{i,1}, channels{j,1});
    end
end
%-----------------------------------

% ---------- opóźnienie plik 2 ------------
channels = cell(5,1);
channels{1,1} = channel1_file2;
channels{2,1} = channel2_file2;
channels{3,1} = channel3_file2;
channels{4,1} = channel4_file2;
channels{5,1} = channel5_file2;
shift_file2 = zeros(5);
for i = 1 : 5
    for j = 1 : 5
        shift_file2(i,j) = finddelay(channels{i,1}, channels{j,1});
    end
end
%-----------------------------------

% ---------- opóźnienie plik 3 ------------
channels = cell(5,1);
channels{1,1} = channel1_file3;
channels{2,1} = channel2_file3;
channels{3,1} = channel3_file3;
channels{4,1} = channel4_file3;
channels{5,1} = channel5_file3;
shift_file3 = zeros(5);
for i = 1 : 5
    for j = 1 : 5
        shift_file3(i,j) = finddelay(channels{i,1}, channels{j,1});
    end
end
%-----------------------------------
        