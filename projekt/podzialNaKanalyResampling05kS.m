% --------- RESAMPLING -----------
load('data.mat');

originalSamplingRate = 8 * 1024;
resampleRate05kS = 0.5 * 1024;

resampledData05kS =  resample(data, resampleRate05kS, originalSamplingRate);

channel1 = resampledData05kS(:,1);
channel2 = resampledData05kS(:,2);
channel3 = resampledData05kS(:,3);
channel4 = resampledData05kS(:,4);
channel5 = resampledData05kS(:,5);
