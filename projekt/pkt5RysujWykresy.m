function pkt5RysujWykresy(channelData, channelName)
%wykres �redniej
figure();
plot(cell2mat(channelData(:,2))); 
title(strcat(channelName, ": ", 'Warto�� �rednia w oknach 1-sekundowych'));
xlabel('Okna 1-sekundowe');
ylabel('Warto��');
grid on;

%wykres wariancji
figure();
plot(cell2mat(channelData(:,3))); 
title(strcat(channelName, ": ", 'Warto�� wariancji w oknach 1-sekundowych'));
xlabel('Okna 1-sekundowe');
ylabel('Warto��');
grid on;

%wykres przej�� przez zero
figure();
plot(cell2mat(channelData(:,4))); 
title(strcat(channelName, ": ", 'Liczba przej�� przez o� X w oknach 1-sekundowych'));
xlabel('Okna 1-sekundowe');
ylabel('Liczba przej��');
grid on;

%wykres przej�� przez zero dla 1 pochodnej
figure();
plot(cell2mat(channelData(:,5))); 
title(strcat(channelName, ": ", 'Liczba przej�� przez o� X w oknach 1-sekundowych dla pierwszej pochodnej'));
xlabel('Okna 1-sekundowe');
ylabel('Liczba przej��');
grid on;
end