podzialNaKanaly;
channel1 = channel1 / 2;
channel2 = channel2 / 2;
channel3 = channel3 / 2;
channel4 = channel4 / 2;
channel5 = channel5 / 2;

samplesCount = size(channel1);
samplesCount = samplesCount(1);
sampleRate = samplesCount / 300;

channel1WindowsRescale1 = mat2cell(channel1, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel2WindowsRescale1 = mat2cell(channel2, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel3WindowsRescale1 = mat2cell(channel3, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel4WindowsRescale1 = mat2cell(channel4, sampleRate*(ones(samplesCount/sampleRate,1)), 1);
channel5WindowsRescale1 = mat2cell(channel5, sampleRate*(ones(samplesCount/sampleRate,1)), 1);

for i = 1 : 300
   %�rednia
   channel1WindowsRescale1{i,2} = mean(channel1WindowsRescale1{i});
   channel2WindowsRescale1{i,2} = mean(channel2WindowsRescale1{i});
   channel3WindowsRescale1{i,2} = mean(channel3WindowsRescale1{i});
   channel4WindowsRescale1{i,2} = mean(channel4WindowsRescale1{i});
   channel5WindowsRescale1{i,2} = mean(channel5WindowsRescale1{i});
   
   %wariancja
   channel1WindowsRescale1{i,3} = var(channel1WindowsRescale1{i});
   channel2WindowsRescale1{i,3} = var(channel2WindowsRescale1{i});
   channel3WindowsRescale1{i,3} = var(channel3WindowsRescale1{i});
   channel4WindowsRescale1{i,3} = var(channel4WindowsRescale1{i});
   channel5WindowsRescale1{i,3} = var(channel5WindowsRescale1{i});   
   
   %liczba przej�� przez zero
   channelMinusMean = channel1WindowsRescale1{i} - mean(channel1WindowsRescale1{i});
   channel1WindowsRescale1{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel2WindowsRescale1{i} - mean(channel2WindowsRescale1{i});
   channel2WindowsRescale1{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel3WindowsRescale1{i} - mean(channel3WindowsRescale1{i});
   channel3WindowsRescale1{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel4WindowsRescale1{i} - mean(channel4WindowsRescale1{i});
   channel4WindowsRescale1{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   channelMinusMean = channel5WindowsRescale1{i} - mean(channel5WindowsRescale1{i});
   channel5WindowsRescale1{i,4} = numel(find(diff(sign(channelMinusMean))));
   
   %liczba przej�� przez zero dla 1 pochodnej
   derivative = diff(channel1WindowsRescale1{i});
   channel1WindowsRescale1{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel2WindowsRescale1{i});
   channel2WindowsRescale1{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel3WindowsRescale1{i});
   channel3WindowsRescale1{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel4WindowsRescale1{i});
   channel4WindowsRescale1{i,5} = numel(find(diff(sign(derivative))));   
   
   derivative = diff(channel5WindowsRescale1{i});
   channel5WindowsRescale1{i,5} = numel(find(diff(sign(derivative))));   
end



